import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import org.gradle.api.tasks.testing.logging.TestLogEvent.*

plugins {
    id("org.jetbrains.kotlin.jvm") version "1.6.10"
    id("org.jetbrains.kotlin.plugin.jpa") version "1.6.10"
    id("org.jetbrains.kotlin.plugin.spring") version "1.6.10"
    id("org.springframework.boot") version "2.6.4"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("com.github.ben-manes.versions") version "0.42.0"
    id("org.shipkit.shipkit-auto-version") version "1.+"
}

group = "com.skaggsm"

tasks.withType<DependencyUpdatesTask> {
    gradleReleaseChannel = "current"
    rejectVersionIf {
        candidate.version.contains("""-M\d+""".toRegex()) ||
                candidate.version.contains("RC")
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

repositories {
    mavenCentral()
}

tasks.bootBuildImage {
    docker {
        val repoUrl = System.getenv("CI_REGISTRY_IMAGE") ?: "git-docker.mst.edu/mlsd45/fossil3d-api"
        imageName = "$repoUrl:latest"
        tags = listOf("$repoUrl:${project.version}")
        publishRegistry {
            username = System.getenv("CI_REGISTRY_USER") ?: "example"
            password = System.getenv("CI_REGISTRY_PASSWORD") ?: "example"
            url = repoUrl
        }
    }
}


dependencyManagement {
    imports {
        mavenBom("io.crnk:crnk-bom:3.4.20210509072026")
    }
}

dependencies {
    // Kotlin
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Spring Boot
    implementation("org.springframework.boot:spring-boot-starter-cache")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-data-rest")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-actuator")

    implementation("io.crnk:crnk-home")
    implementation("io.crnk:crnk-data-jpa")
    implementation("io.crnk:crnk-setup-spring-boot2")

    // Spring other
    implementation("com.h2database:h2")
    implementation("org.mariadb.jdbc:mariadb-java-client")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.hibernate:hibernate-jcache")
    implementation("org.ehcache:ehcache")

    compileOnly("org.springframework.boot:spring-boot-configuration-processor")

    runtimeOnly("org.springframework.boot:spring-boot-devtools")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.2")
    testRuntimeOnly("org.junit.vintage:junit-vintage-engine:5.8.2")
}

tasks.compileKotlin {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}
tasks.compileTestKotlin {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}
tasks.test {
    useJUnitPlatform()
    testLogging {
        events(PASSED, SKIPPED, FAILED)
    }
}

val createCurrentTag by tasks.registering(Exec::class) {
    commandLine("git", "tag", "v${project.version}")
}

val pushCurrentTag by tasks.registering(Exec::class) {
    dependsOn(createCurrentTag)
    commandLine("git", "push", "origin", "v${project.version}")
}
