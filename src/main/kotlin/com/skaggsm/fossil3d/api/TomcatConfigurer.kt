package com.skaggsm.fossil3d.api

import org.apache.coyote.http2.Http2Protocol
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory
import org.springframework.boot.web.server.WebServerFactoryCustomizer
import org.springframework.stereotype.Component

/**
 * Created by Mitchell Skaggs on 11/19/2019.
 */
@Component
class TomcatConfigurer : WebServerFactoryCustomizer<TomcatServletWebServerFactory> {
    override fun customize(factory: TomcatServletWebServerFactory) {
        // Add H2C upgrade support for all connectors
        factory.addConnectorCustomizers(TomcatConnectorCustomizer { it.addUpgradeProtocol(Http2Protocol()) })
    }
}
