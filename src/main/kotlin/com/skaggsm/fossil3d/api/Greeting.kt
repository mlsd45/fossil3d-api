package com.skaggsm.fossil3d.api

/**
 * Created by Mitchell Skaggs on 2/20/2019.
 */
data class Greeting(val content: String)