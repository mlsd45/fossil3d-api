package com.skaggsm.fossil3d.api

import io.crnk.core.boot.CrnkBoot
import io.crnk.core.queryspec.pagingspec.NumberSizePagingBehavior
import io.crnk.spring.setup.boot.core.CrnkBootConfigurer
import org.springframework.stereotype.Component

/**
 * Created by Mitchell Skaggs on 8/23/2019.
 */
@Component
class CrnkBootConfigurer : CrnkBootConfigurer {
    override fun configure(boot: CrnkBoot) {
        boot.webPathPrefix = "/api/v2"
        boot.setDefaultPageLimit(20)
        boot.setMaxPageLimit(1000)
        boot.moduleRegistry.addPagingBehavior(NumberSizePagingBehavior())
    }
}
