package com.skaggsm.fossil3d.api

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FossilApiApplication

fun main(args: Array<String>) {
    runApplication<FossilApiApplication>(*args)
}
