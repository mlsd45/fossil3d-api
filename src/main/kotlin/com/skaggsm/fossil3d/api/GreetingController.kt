package com.skaggsm.fossil3d.api

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


/**
 * Created by Mitchell Skaggs on 2/20/2019.
 */
@RestController
class GreetingController {

    @GetMapping("/greeting")
    fun greeting(@RequestParam(value = "name", defaultValue = "World") name: String): Greeting {
        return Greeting(String.format(template, name))
    }

    companion object {
        private const val template = "Hello, %s!"
    }
}