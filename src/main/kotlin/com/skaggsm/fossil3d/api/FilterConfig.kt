package com.skaggsm.fossil3d.api

import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.http.CacheControl
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import org.springframework.web.filter.ShallowEtagHeaderFilter
import java.util.concurrent.TimeUnit.MINUTES
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Created by Mitchell Skaggs on 7/19/2019.
 */
@Component
class FilterConfig {
    @Bean
    fun etagFilter(): FilterRegistrationBean<ShallowEtagHeaderFilter> {
        return FilterRegistrationBean<ShallowEtagHeaderFilter>().apply {
            this.filter = ShallowEtagHeaderFilter()
            this.filter.isWriteWeakETag = true
            this.order = 1
        }
    }

    /**
     * Disabled due to issues with feature (sending blank response).
     */
    //@Bean
    fun cacheControlFilter(): FilterRegistrationBean<CacheControlFilter> {
        return FilterRegistrationBean<CacheControlFilter>().apply {
            this.filter = CacheControlFilter
            this.order = 2
        }
    }

    object CacheControlFilter : OncePerRequestFilter() {
        override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
            response.addHeader("Cache-Control", CacheControl.noCache().cachePublic().staleIfError(10, MINUTES).staleWhileRevalidate(10, MINUTES).headerValue)
        }
    }
}
