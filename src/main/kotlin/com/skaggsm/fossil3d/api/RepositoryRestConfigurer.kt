package com.skaggsm.fossil3d.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer
import org.springframework.stereotype.Component
import org.springframework.web.servlet.config.annotation.CorsRegistry
import javax.persistence.EntityManager

/**
 * Created by Mitchell Skaggs on 3/2/2019.
 */
@Component
class RepositoryRestConfigurer(@Autowired val entityManager: EntityManager) : RepositoryRestConfigurer {
    override fun configureRepositoryRestConfiguration(config: RepositoryRestConfiguration, cors: CorsRegistry) {
        cors.addMapping("/api/**")

        config.exposeIdsFor(*entityManager.metamodel.entities.map { it.javaType }.toTypedArray())
    }
}
