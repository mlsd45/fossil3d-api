package com.skaggsm.fossil3d.api.model.geology.temporal.divisions

import com.skaggsm.fossil3d.api.model.geology.stratigraphy.RockUnit
import com.skaggsm.fossil3d.api.model.geology.temporal.scales.TimeScale
import io.crnk.core.resource.annotations.JsonApiResource
import javax.persistence.*

/**
 * Created by Mitchell Skaggs on 2/24/2019.
 */
@JsonApiResource(type = "timeDivision", patchable = false, postable = false, deletable = false)
@Entity
data class TimeDivision(
        @Id
        val name: String,

        @ManyToOne
        @JoinTable(name = "time_division_time_scale")
        val scale: TimeScale,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinTable(name = "time_division_parent")
        val parent: TimeDivision? = null
) {
    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    val children: List<TimeDivision> = emptyList()

    @ManyToMany(mappedBy = "times", fetch = FetchType.LAZY)
    val rockUnits: List<RockUnit> = emptyList()
}
