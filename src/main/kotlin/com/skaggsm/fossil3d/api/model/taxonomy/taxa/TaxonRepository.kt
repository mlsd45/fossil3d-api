package com.skaggsm.fossil3d.api.model.taxonomy.taxa

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

/**
 * Created by Mitchell Skaggs on 2/23/2019.
 */
@RepositoryRestResource(collectionResourceRel = "taxa", path = "taxa", excerptProjection = TaxonWithRankOrdering::class)
interface TaxonRepository : PagingAndSortingRepository<Taxon, String> {
    @RestResource(exported = false)
    override fun <S : Taxon?> save(entity: S): S

    @RestResource(exported = false)
    override fun <S : Taxon?> saveAll(entities: MutableIterable<S>): MutableIterable<S>

    @RestResource(exported = false)
    override fun delete(entity: Taxon)

    @RestResource(exported = false)
    override fun deleteById(id: String)

    @RestResource(exported = false)
    override fun deleteAll()

    @RestResource(exported = false)
    override fun deleteAll(entities: MutableIterable<Taxon>)
}