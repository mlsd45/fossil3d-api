package com.skaggsm.fossil3d.api.model.db

import com.skaggsm.fossil3d.api.model.geology.samples.fossils.Fossil
import com.skaggsm.fossil3d.api.model.geology.samples.fossils.FossilRepository
import com.skaggsm.fossil3d.api.model.geology.stratigraphy.RockUnit
import com.skaggsm.fossil3d.api.model.geology.stratigraphy.RockUnitRepository
import com.skaggsm.fossil3d.api.model.geology.temporal.divisions.TimeDivision
import com.skaggsm.fossil3d.api.model.geology.temporal.divisions.TimeDivisionRepository
import com.skaggsm.fossil3d.api.model.geology.temporal.scales.TimeScale
import com.skaggsm.fossil3d.api.model.geology.temporal.scales.TimeScaleRepository
import com.skaggsm.fossil3d.api.model.taxonomy.ranks.Rank
import com.skaggsm.fossil3d.api.model.taxonomy.ranks.RankRepository
import com.skaggsm.fossil3d.api.model.taxonomy.taxa.Taxon
import com.skaggsm.fossil3d.api.model.taxonomy.taxa.TaxonRepository
import org.slf4j.Logger
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import org.springframework.util.StopWatch
import javax.transaction.Transactional

/**
 * Created by Mitchell Skaggs on 2/23/2019.
 */
@Component
class DatabaseInitializer(
        private val fossilRepository: FossilRepository,
        private val taxonRepository: TaxonRepository,
        private val rankRepository: RankRepository,
        private val rockUnitRepository: RockUnitRepository,
        private val timeDivisionRepository: TimeDivisionRepository,
        private val timeScaleRepository: TimeScaleRepository,
        private val logger: Logger
) : CommandLineRunner {
    @Suppress("UNUSED_VARIABLE", "LocalVariableName")
    @Transactional
    override fun run(vararg args: String?) {

        val watch = StopWatch("DB init")

        // Timescales
        watch.start("Init timescales")
        logger.info("Init timescales start")

        val supereon = timeScaleRepository.save(TimeScale("supereon", 100000))
        val eon = timeScaleRepository.save(TimeScale("eon", 10000))
        val era = timeScaleRepository.save(TimeScale("era", 1000))
        val period = timeScaleRepository.save(TimeScale("period", 100))
        val subperiod = timeScaleRepository.save(TimeScale("subperiod", 50))
        val epoch = timeScaleRepository.save(TimeScale("epoch", 10))
        val age = timeScaleRepository.save(TimeScale("age", 1))

        watch.stop()
        logger.info("Init timescales end")

        // Ranks
        watch.start("Init ranks")
        logger.info("Init ranks start")

        val domain = rankRepository.save(Rank("domain", 10000000))
        val kingdom = rankRepository.save(Rank("kingdom", 1000000))
        val phylum = rankRepository.save(Rank("phylum", 100000))
        val subphylum = rankRepository.save(Rank("subphylum", 50000))
        val klass = rankRepository.save(Rank("class", 10000))
        val order = rankRepository.save(Rank("order", 1000))
        val family = rankRepository.save(Rank("family", 100))
        val genus = rankRepository.save(Rank("genus", 10))
        val species = rankRepository.save(Rank("species", 1))

        watch.stop()
        logger.info("Init ranks end")

        // Time divisions
        watch.start("Init time divisions")
        logger.info("Init time divisions start")

        // Supereons
        val precambrian = timeDivisionRepository.save(TimeDivision("precambrian", supereon))

        // Eons
        val hadean = timeDivisionRepository.save(TimeDivision("hadean", eon, precambrian))
        val archean = timeDivisionRepository.save(TimeDivision("archean", eon, precambrian))
        val proterozoic = timeDivisionRepository.save(TimeDivision("proterozoic", eon, precambrian))
        val phanerozoic = timeDivisionRepository.save(TimeDivision("phanerozoic", eon))

        // Eras
        val paleozoic = timeDivisionRepository.save(TimeDivision("paleozoic", era, phanerozoic))
        val cenozoic = timeDivisionRepository.save(TimeDivision("cenozoic", era, phanerozoic))

        // Periods
        val carboniferous = timeDivisionRepository.save(TimeDivision("carboniferous", period, paleozoic))
        val paleogene = timeDivisionRepository.save(TimeDivision("paleogene", period, cenozoic))

        // Subperiods
        val pennsylvanian = timeDivisionRepository.save(TimeDivision("pennsylvanian", subperiod, carboniferous))

        // Epochs
        val eocene = timeDivisionRepository.save(TimeDivision("eocene", epoch, paleogene))
        val quaternary = timeDivisionRepository.save(TimeDivision("quaternary", epoch, cenozoic))

        watch.stop()
        logger.info("Init time divisions end")

        // Taxa
        watch.start("Init taxa")
        logger.info("Init taxa start")

        // Domains
        val archaea = taxonRepository.save(Taxon("archaea", domain))
        val bacteria = taxonRepository.save(Taxon("bacteria", domain))
        val eukarya = taxonRepository.save(Taxon("eukarya", domain))

        // Kingdoms
        val protozoa = taxonRepository.save(Taxon("protozoa", kingdom, eukarya))
        val chromista = taxonRepository.save(Taxon("chromista", kingdom, eukarya))
        val plantae = taxonRepository.save(Taxon("plantae", kingdom, eukarya))
        val fungi = taxonRepository.save(Taxon("fungi", kingdom, eukarya))
        val animalia = taxonRepository.save(Taxon("animalia", kingdom, eukarya))

        // Phyla
        val pteridophyta = taxonRepository.save(Taxon("pteridophyta", phylum, plantae))
        val mollusca = taxonRepository.save(Taxon("mollusca", phylum, animalia))
        val spermatophyta = taxonRepository.save(Taxon("spermatophyta", phylum, plantae))
        val chordata = taxonRepository.save(Taxon("chordata", phylum, animalia))
        val cnidaria = taxonRepository.save(Taxon("cnidaria", phylum, animalia))
        val porifera = taxonRepository.save(Taxon("porifera", phylum, animalia))
        val brachiopoda = taxonRepository.save(Taxon("brachiopoda", phylum, animalia))
        val bryozoa = taxonRepository.save(Taxon("bryozoa", phylum, animalia))
        val arthropoda = taxonRepository.save(Taxon("arthropoda", phylum, animalia))
        val echinodermata = taxonRepository.save(Taxon("echinodermata", phylum, animalia))

        // Subphyla
        val vertebrata = taxonRepository.save(Taxon("vertebrata", subphylum, chordata))

        // Classes
        val filicopsida = taxonRepository.save(Taxon("filicopsida", klass, pteridophyta))
        val tergomya = taxonRepository.save(Taxon("tergomya", klass, mollusca))
        val magnoliopsida = taxonRepository.save(Taxon("magnoliopsida", klass, spermatophyta))
        val actinopteri = taxonRepository.save(Taxon("actinopteri", klass, vertebrata))
        val anthozoa = taxonRepository.save(Taxon("anthozoa", klass, cnidaria))
        val bivalvia = taxonRepository.save(Taxon("bivalvia", klass, mollusca))
        val demospongea = taxonRepository.save(Taxon("demospongea", klass, porifera))
        val hexactinellida = taxonRepository.save(Taxon("hexactinellida", klass, porifera))
        val rhynchonellata = taxonRepository.save(Taxon("rhynchonellata", klass, brachiopoda))
        val lingulata = taxonRepository.save(Taxon("lingulata", klass, brachiopoda))
        val strophomenata = taxonRepository.save(Taxon("strophomenata", klass, brachiopoda))
        val stenolaemata = taxonRepository.save(Taxon("stenolaemata ", klass, bryozoa))
        val trilobita = taxonRepository.save(Taxon("trilobita", klass, arthropoda))
        val maxillopoda = taxonRepository.save(Taxon("maxillopoda", klass, arthropoda))
        val scaphopoda = taxonRepository.save(Taxon("scaphopoda", klass, mollusca))
        val gastropoda = taxonRepository.save(Taxon("gastropoda", klass, mollusca))
        val cephalopoda = taxonRepository.save(Taxon("cephalopoda", klass, mollusca))
        val crinoidea = taxonRepository.save(Taxon("crinoidea", klass, echinodermata))
        val echinoidea = taxonRepository.save(Taxon("echinoidea", klass, echinodermata))
        val asteroidea = taxonRepository.save(Taxon("asteroidea", klass, echinodermata))
        val holothuroidea = taxonRepository.save(Taxon("holothuroidea", klass, echinodermata))

        // Orders
        val marattiales = taxonRepository.save(Taxon("marattiales", order, filicopsida))
        val tryblidiida = taxonRepository.save(Taxon("tryblidiida", order, tergomya))
        val ellimmichthyiformes = taxonRepository.save(Taxon("ellimmichthyiformes", order, actinopteri))
        val scleractinia = taxonRepository.save(Taxon("scleractinia", order, anthozoa))
        val unionida = taxonRepository.save(Taxon("unionida", order, bivalvia))
        val pectinida = taxonRepository.save(Taxon("pectinida", order, bivalvia))
        val agelasida = taxonRepository.save(Taxon("agelasida", order, demospongea))
        val reticulosa = taxonRepository.save(Taxon("reticulosa", order, hexactinellida))
        val stauriida = taxonRepository.save(Taxon("stauriida", order, anthozoa))
        val auloporida = taxonRepository.save(Taxon("auloporida", order, anthozoa))
        val terebratulida = taxonRepository.save(Taxon("terebratulida", order, rhynchonellata))
        val lingulida = taxonRepository.save(Taxon("lingulida", order, lingulata))
        val spiriferida = taxonRepository.save(Taxon("spiriferida", order, rhynchonellata))
        val productida = taxonRepository.save(Taxon("productida", order, strophomenata))
        val orthida = taxonRepository.save(Taxon("orthida", order, rhynchonellata))
        val strophomenida = taxonRepository.save(Taxon("strophomenida", order, strophomenata))
        val rhynchonellida = taxonRepository.save(Taxon("rhynchonellida", order, rhynchonellata))
        val cyclostomata = taxonRepository.save(Taxon("cyclostomata", order, stenolaemata))
        val cystoporida = taxonRepository.save(Taxon("cystoporida", order, stenolaemata))
        val fenestrida = taxonRepository.save(Taxon("fenestrida", order, stenolaemata))
        val corynexochida = taxonRepository.save(Taxon("corynexochida", order, trilobita))
        val ptychopariida = taxonRepository.save(Taxon("ptychopariida", order, trilobita))
        val agnostida = taxonRepository.save(Taxon("agnostida", order, trilobita))
        val asaphida = taxonRepository.save(Taxon("asaphida", order, trilobita))
        val phacopida = taxonRepository.save(Taxon("phacopida", order, trilobita))
        val sessilia = taxonRepository.save(Taxon("sessilia", order, maxillopoda))
        val dentaliida = taxonRepository.save(Taxon("dentaliida", order, scaphopoda))
        val neogastropoda = taxonRepository.save(Taxon("neogastropoda", order, gastropoda))
        val euomphalina = taxonRepository.save(Taxon("neogastropoda", order, gastropoda))
        val lepetellida = taxonRepository.save(Taxon("lepetellida", order, gastropoda))
        val littorinimorpha = taxonRepository.save(Taxon("littorinimorpha", order, gastropoda))
        val caenogastropoda = taxonRepository.save(Taxon("caenogastropoda", order, gastropoda))
        val venerida = taxonRepository.save(Taxon("venerida", order, bivalvia))
        val arcida = taxonRepository.save(Taxon("arcida", order, bivalvia))
        val ostreida = taxonRepository.save(Taxon("ostreida", order, bivalvia))
        val mytilida = taxonRepository.save(Taxon("mytilida", order, bivalvia))
        val belemnitida = taxonRepository.save(Taxon("belemnitida", order, cephalopoda))
        val ammonitida = taxonRepository.save(Taxon("ammonitida", order, cephalopoda))
        val monobathrida = taxonRepository.save(Taxon("monobathrida", order, crinoidea))
        val clypeasteroida = taxonRepository.save(Taxon("clypeasteroida", order, echinoidea))
        val velatida = taxonRepository.save(Taxon("velatida", order, asteroidea))
        val dendrochirotida = taxonRepository.save(Taxon("dendrochirotida", order, holothuroidea))

        // Families
        val marattiaceae = taxonRepository.save(Taxon("marattiaceae", family, marattiales))
        val tryblidiidae = taxonRepository.save(Taxon("tryblidiidae", family, tryblidiida))
        val ellimmichthyidae = taxonRepository.save(Taxon("ellimmichthyidae", family, ellimmichthyiformes))
        val unionidae = taxonRepository.save(Taxon("unionidae", family, unionida))
        val pectinidae = taxonRepository.save(Taxon("pectinidae", family, pectinida))
        val sebargasiidae = taxonRepository.save(Taxon("sebargasiidae", family, agelasida))
        val fungiidae = taxonRepository.save(Taxon("fungiidae", family, scleractinia))
        val zaphrentidae = taxonRepository.save(Taxon("zaphrentidae", family, stauriida))
        val syringoporidae = taxonRepository.save(Taxon("syringoporidae", family, auloporida))
        val disphyllidae = taxonRepository.save(Taxon("disphyllidae", family, stauriida))
        val terebratellidae = taxonRepository.save(Taxon("terebratellidae", family, terebratulida))
        val lingulidae = taxonRepository.save(Taxon("lingulidae", family, lingulida))
        val hysterolitidae = taxonRepository.save(Taxon("hysterolitidae", family, spiriferida))
        val productidae = taxonRepository.save(Taxon("productidae", family, productida))
        val plectorthidae = taxonRepository.save(Taxon("plectorthidae", family, orthida))
        val rafinesquinidae = taxonRepository.save(Taxon("rafinesquinidae", family, strophomenida))
        val hederellidae = taxonRepository.save(Taxon("hederellidae", family, cyclostomata))
        val constellariidae = taxonRepository.save(Taxon("constellariidae", family, cystoporida))
        val fenestellidae = taxonRepository.save(Taxon("fenestellidae", family, fenestrida))
        val dolichometopidae = taxonRepository.save(Taxon("dolichometopidae", family, corynexochida))
        val alokistocaridae = taxonRepository.save(Taxon("alokistocaridae", family, ptychopariida))
        val peronopsidae = taxonRepository.save(Taxon("peronopsidae", family, agnostida))
        val dikelocephalidae = taxonRepository.save(Taxon("dikelocephalidae", family, asaphida))
        val phacopidae = taxonRepository.save(Taxon("phacopidae", family, phacopida))
        val calymenidae = taxonRepository.save(Taxon("calymenidae", family, phacopida))
        val balanidae = taxonRepository.save(Taxon("balanidae", family, sessilia))
        val dentaliidae = taxonRepository.save(Taxon("dentaliidae", family, dentaliida))
        val buccinidae = taxonRepository.save(Taxon("buccinidae", family, neogastropoda))
        val euomphalidae = taxonRepository.save(Taxon("euomphalidae", family, euomphalina))
        val fissurellidae = taxonRepository.save(Taxon("fissurellidae", family, lepetellida))
        val olividae = taxonRepository.save(Taxon("olividae", family, neogastropoda))
        val naticidae = taxonRepository.save(Taxon("naticidae", family, littorinimorpha))
        val turritellidae = taxonRepository.save(Taxon("turritellidae", family, caenogastropoda))
        val veneridae = taxonRepository.save(Taxon("veneridae", family, venerida))
        val arcidae = taxonRepository.save(Taxon("arcidae", family, arcida))
        val gryphaeidae = taxonRepository.save(Taxon("gryphaeidae", family, ostreida))
        val mytilidae = taxonRepository.save(Taxon("mytilidae", family, mytilida))
        val mactridae = taxonRepository.save(Taxon("mactridae", family, venerida))
        val baculitidae = taxonRepository.save(Taxon("baculitidae", family, ammonitida))
        val scaphitidae = taxonRepository.save(Taxon("scaphitidae", family, ammonitida))
        val clypeasteridae = taxonRepository.save(Taxon("clypeasteridae", family, clypeasteroida))
        val pterasteridae = taxonRepository.save(Taxon("pterasteridae", family, velatida))
        val phyllophoridae = taxonRepository.save(Taxon("phyllophoridae", family, dendrochirotida))
        val echinarachniidae = taxonRepository.save(Taxon("echinarachniidae", family, clypeasteroida))

        // Genera
        val pecopteris = taxonRepository.save(Taxon("pecopteris", genus, marattiaceae))
        val proplina = taxonRepository.save(Taxon("proplina", genus, tryblidiidae))
        val diplomystus = taxonRepository.save(Taxon("diplomystus", genus, ellimmichthyidae))
        val unio = taxonRepository.save(Taxon("unio", genus, unionidae))
        val pecten = taxonRepository.save(Taxon("pecten", genus, pectinidae))
        val amblysiphonella = taxonRepository.save(Taxon("amblysiphonella", genus, sebargasiidae))
        val hydnoceras = taxonRepository.save(Taxon("hydnoceras", genus, reticulosa))
        val fungia = taxonRepository.save(Taxon("fungia", genus, fungiidae))
        val zaphrentis = taxonRepository.save(Taxon("zaphrentis", genus, zaphrentidae))
        val syringipora = taxonRepository.save(Taxon("syringipora", genus, syringoporidae))
        val hexagonaria = taxonRepository.save(Taxon("hexagonaria", genus, disphyllidae))
        val terebratella = taxonRepository.save(Taxon("terebratella", genus, terebratellidae))
        val lingula = taxonRepository.save(Taxon("lingula", genus, lingulidae))
        val paraspirifer = taxonRepository.save(Taxon("paraspirifer", genus, hysterolitidae))
        val dictyoclostus = taxonRepository.save(Taxon("dictyoclostus", genus, productidae))
        val hebertella = taxonRepository.save(Taxon("hebertella", genus, plectorthidae))
        val rafinesquina = taxonRepository.save(Taxon("rafinesquina", genus, rafinesquinidae))
        val orthorhyncula = taxonRepository.save(Taxon("orthorhyncula", genus, rhynchonellida))
        val hederella = taxonRepository.save(Taxon("hederella", genus, hederellidae))
        val constellaria = taxonRepository.save(Taxon("constellaria", genus, constellariidae))
        val archimedes = taxonRepository.save(Taxon("archimedes", genus, fenestellidae))
        val bathyuriscus = taxonRepository.save(Taxon("bathyuriscus", genus, dolichometopidae))
        val elrathia = taxonRepository.save(Taxon("elrathia", genus, alokistocaridae))
        val peronopsis = taxonRepository.save(Taxon("peronopsis", genus, peronopsidae))
        val dikelocephalus = taxonRepository.save(Taxon("dikelocephalus", genus, dikelocephalidae))
        val eldredgeops = taxonRepository.save(Taxon("eldredgeops", genus, phacopidae))
        val flexicalymene = taxonRepository.save(Taxon("flexicalymene", genus, calymenidae))
        val balanus = taxonRepository.save(Taxon("balanus", genus, balanidae))
        val dentalium = taxonRepository.save(Taxon("dentalium", genus, dentaliidae))
        val busycon = taxonRepository.save(Taxon("busycon", genus, buccinidae))
        val amphiscapha = taxonRepository.save(Taxon("amphiscapha", genus, euomphalidae))
        val fissurella = taxonRepository.save(Taxon("fissurella", genus, fissurellidae))
        val oliva = taxonRepository.save(Taxon("oliva", genus, olividae))
        val polinices = taxonRepository.save(Taxon("polinices", genus, naticidae))
        val turritella = taxonRepository.save(Taxon("turritella", genus, turritellidae))
        val venus = taxonRepository.save(Taxon("venus", genus, veneridae))
        val arca = taxonRepository.save(Taxon("arca", genus, arcidae))
        val exogyra = taxonRepository.save(Taxon("exogyra", genus, gryphaeidae))
        val truncilla = taxonRepository.save(Taxon("truncilla", genus, unionidae))
        val mytilus = taxonRepository.save(Taxon("mytilus", genus, mytilidae))
        val mactra = taxonRepository.save(Taxon("mactra", genus, mactridae))
        val pachyteuthis = taxonRepository.save(Taxon("pachyteuthis", genus, belemnitida))
        val baculites = taxonRepository.save(Taxon("baculites", genus, baculitidae))
        val acanthoscaphites = taxonRepository.save(Taxon("acanthoscaphites", genus, scaphitidae))
        val dorycrinus = taxonRepository.save(Taxon("dorycrinus", genus, monobathrida))
        val cactocrinus = taxonRepository.save(Taxon("cactocrinus", genus, monobathrida))
        val orophocrinus = taxonRepository.save(Taxon("orophocrinus", genus, monobathrida))
        val hempineustes = taxonRepository.save(Taxon("hempineustes", genus))
        val diplothecanthus = taxonRepository.save(Taxon("diplothecanthus", genus, clypeasteridae))
        val pteraster = taxonRepository.save(Taxon("pteraster", genus, pterasteridae))
        val thyone = taxonRepository.save(Taxon("thyone", genus, phyllophoridae))
        val echinarachnius = taxonRepository.save(Taxon("echinarachnius", genus, echinarachniidae))

        // Species
        val hemitelioides = taxonRepository.save(Taxon("hemitelioides", species, pecopteris))
        val complanatus = taxonRepository.save(Taxon("complanatus", species, unio))
        val hericeus = taxonRepository.save(Taxon("hericeus", species, pecten))
        val cerronensis = taxonRepository.save(Taxon("cerronensis", species, pecten))
        val canadensis = taxonRepository.save(Taxon("canadensis", species, hederella))
        val wortheni = taxonRepository.save(Taxon("wortheni", species, archimedes))
        val formosus = taxonRepository.save(Taxon("formosus", species, bathyuriscus))
        val interstricus = taxonRepository.save(Taxon("interstricus", species, peronopsis))
        val minnesoten = taxonRepository.save(Taxon("minnesoten", species, dikelocephalus))
        val rana = taxonRepository.save(Taxon("rana", species, eldredgeops))
        val meeki = taxonRepository.save(Taxon("meeki", species, flexicalymene))

        watch.stop()
        logger.info("Init taxa end")

        // Lab 2 Q1A
        watch.start("Init lab2_q1a")
        logger.info("Init lab2_q1a start")

        // Rock units
        val francisCreekShale = rockUnitRepository.save(RockUnit(
                "francis_creek_shale",
                "Illinois",
                listOf(
                        phanerozoic,
                        paleozoic,
                        carboniferous,
                        pennsylvanian
                )
        ))

        // Fossils
        val lab2_q1a = fossilRepository.save(Fossil(
                "lab2_q1a",
                "Lab 2 Question 1A Leaf",
                "cb83b93359ce4577a0f29e10b5fe52f9",
                hemitelioides,
                francisCreekShale
        ))

        watch.stop()
        logger.info("Init lab2_q1a end")

        // Lab 2 Q1B
        watch.start("Init lab2_q1b")
        logger.info("Init lab2_q1b start")

        // Fossils
        val lab2_q1b = fossilRepository.save(Fossil(
                "lab2_q1b",
                "Lab 2 Question 1B Mollusk",
                "a54052e2f1c241ffa80fd8724ab98f20",
                proplina
        ))

        watch.stop()
        logger.info("Init lab2_q1b end")

        // Lab 2 Q1C
        watch.start("Init lab2_q1c")
        logger.info("Init lab2_q1c start")

        // Fossils
        val lab2_q1c = fossilRepository.save(Fossil(
                "lab2_q1c",
                "Lab 2 Question 1C Wood",
                "8df286a3768e4cdbbe592a3c3b60f7d2",
                magnoliopsida
        ))

        watch.stop()
        logger.info("Init lab2_q1c end")

        // Lab 2 Q1D
        watch.start("Init lab2_q1d")
        logger.info("Init lab2_q1d start")

        // Rock units
        val greenRiverFormation = rockUnitRepository.save(RockUnit(
                "green_river_formation",
                "Wyoming",
                listOf(
                        phanerozoic,
                        cenozoic,
                        paleogene,
                        eocene
                )
        ))

        // Fossils
        val lab2_q1d = fossilRepository.save(Fossil(
                "lab2_q1d",
                "Lab 2 Question 1D Fish",
                "ce42bbbc42dc4bbe935f65017a0758f4",
                diplomystus,
                greenRiverFormation
        ))

        watch.stop()
        logger.info("Init lab2_q1d end")

        // Lab 2 Q2
        watch.start("Init lab2_q2")
        logger.info("Init lab2_q2 start")

        // Rock units
        val unknownQuaternaryUnit = rockUnitRepository.save(RockUnit(
                "unknown_quaternary",
                "Unknown",
                listOf(
                        phanerozoic,
                        cenozoic,
                        quaternary
                )
        ))

        // Fossils
        val lab2_q2 = fossilRepository.save(Fossil(
                "lab2_q2",
                "Lab 2 Question 2 Coral",
                "86538793be194b7bb5c51252d0684b16",
                scleractinia,
                unknownQuaternaryUnit
        ))

        watch.stop()
        logger.info("Init lab2_q2 end")

        // Lab 2 Q3A
        watch.start("Init lab2_q3a")
        logger.info("Init lab2_q3a start")

        // Fossils
        val lab2_q3a = fossilRepository.save(Fossil(
                "lab2_q3a",
                "Lab 2 Question 3 Slab 1",
                "864d3b088b47459098d90f8b7478b91e"
        ))

        watch.stop()
        logger.info("Init lab2_q3a end")

        // Lab 2 Q3B
        watch.start("Init lab2_q3b")
        logger.info("Init lab2_q3b start")

        // Fossils
        val lab2_q3b = fossilRepository.save(Fossil(
                "lab2_q3b",
                "Lab 2 Question 3 Slab 2",
                null
        ))

        watch.stop()
        logger.info("Init lab2_q3b end")

        // Lab 3 Q5B
        watch.start("Init lab3_q5b")
        logger.info("Init lab3_q5b start")

        // Fossils
        val lab3_q5b = fossilRepository.save(Fossil(
                "lab3_q5b",
                "Lab 3 Question 5B Unio complanatus",
                "6c71d334149f492aadb84ec3343f3e36",
                complanatus
        ))

        watch.stop()
        logger.info("Init lab3_q5b end")

        // Lab 3 Q6A
        watch.start("Init lab3_q6a")
        logger.info("Init lab3_q6a start")

        // Fossils
        val lab3_q6a = fossilRepository.save(Fossil(
                "lab3_q6a",
                "Lab 3 Question 6A Pecten hericeus",
                "06513e8c83044de8a972b1669deb0815",
                hericeus
        ))

        watch.stop()
        logger.info("Init lab3_q6a end")

        // Lab 3 Q6B
        watch.start("Init lab3_q6b")
        logger.info("Init lab3_q6b start")

        // Fossils
        val lab3_q6b = fossilRepository.save(Fossil(
                "lab3_q6b",
                "Lab 3 Question 6B Pecten cerronensis",
                "b54f0725ba144d1a9508ff1956b971da",
                cerronensis
        ))

        watch.stop()
        logger.info("Init lab3_q6b end")

        // Lab 3 Q6C
        watch.start("Init lab3_q6c")
        logger.info("Init lab3_q6c start")

        // Fossils
        val lab3_q6c = fossilRepository.save(Fossil(
                "lab3_q6c",
                "Lab 3 Question 6C Bivalve indet",
                "77afda8bc43a48529aad660646c4f29e",
                bivalvia
        ))

        watch.stop()
        logger.info("Init lab3_q6c end")

        // Lab 6 Q2A
        watch.start("Init lab6_q2a")
        logger.info("Init lab6_q2a start")

        // Fossils
        val lab6_q2a = fossilRepository.save(Fossil(
                "lab6_q2a",
                "Lab 6 Question 2A Amblysiphonella",
                "8d2aa21f2ee442a895a2998eadd8206b",
                amblysiphonella
        ))

        watch.stop()
        logger.info("Init lab6_q2a end")

        // Lab 6 Q2C
        watch.start("Init lab6_q2c")
        logger.info("Init lab6_q2c start")

        // Fossils
        val lab6_q2c = fossilRepository.save(Fossil(
                "lab6_q2c",
                "Lab 6 Question 2C Hydnoceras",
                "4fbb09ebe6e5437ebf110ce824fed8f7",
                hydnoceras
        ))

        watch.stop()
        logger.info("Init lab6_q2c end")

        // Lab 6 Q5A
        watch.start("Init lab6_q5a")
        logger.info("Init lab6_q5a start")

        // Fossils
        val lab6_q5a = fossilRepository.save(Fossil(
                "lab6_q5a",
                "Lab 6 Question 5A Fungia",
                "169f8000ce3a45958b1f8758c3adb95c",
                fungia
        ))

        watch.stop()
        logger.info("Init lab6_q5a end")

        // Lab 6 Q5B
        watch.start("Init lab6_q5b")
        logger.info("Init lab6_q5b start")

        // Fossils
        val lab6_q5b = fossilRepository.save(Fossil(
                "lab6_q5b",
                "Lab 6 Question 5B Zaphrentis",
                "e972c78215144d4baec8cdc035d6183a",
                zaphrentis
        ))

        watch.stop()
        logger.info("Init lab6_q5b end")

        // Lab 6 Q5C
        watch.start("Init lab6_q5c")
        logger.info("Init lab6_q5c start")

        // Fossils
        val lab6_q5c = fossilRepository.save(Fossil(
                "lab6_q5c",
                "Lab 6 Question 5C Syringipora",
                "fd78400403714c3e93f65f1ccd19ab64",
                syringipora
        ))

        watch.stop()
        logger.info("Init lab6_q5c end")

        // Lab 6 Q5D
        watch.start("Init lab6_q5d")
        logger.info("Init lab6_q5d start")

        // Fossils
        val lab6_q5d = fossilRepository.save(Fossil(
                "lab6_q5d",
                "Lab 6 Question 5D Hexagonaria",
                "5c6274b94cca4fecaf17c689f65314c4",
                stauriida
        ))

        watch.stop()
        logger.info("Init lab6_q5d end")

        // Lab 7 Q1
        watch.start("Init lab7_q1")
        logger.info("Init lab7_q1 start")

        // Fossils
        val lab7_q1 = fossilRepository.save(Fossil(
                "lab7_q1",
                "Lab 7 Question 1 Terebratella",
                "9943c3404409499b8ccc7fea38bfd81f",
                terebratella
        ))

        watch.stop()
        logger.info("Init lab7_q1 end")

        // Lab 7 Q3A
        watch.start("Init lab7_q3a")
        logger.info("Init lab7_q3a start")

        // Fossils
        val lab7_q3a = fossilRepository.save(Fossil(
                "lab7_q3a",
                "Lab 7 Question 3A Lingula",
                "915620c4ff17441e8176c065e4d5f44c",
                lingula
        ))

        watch.stop()
        logger.info("Init lab7_q3a end")

        // Lab 7 Q3B
        watch.start("Init lab7_q3b")
        logger.info("Init lab7_q3b start")

        // Fossils
        val lab7_q3b = fossilRepository.save(Fossil(
                "lab7_q3b",
                "Lab 7 Question 3B Paraspirifer",
                "c3eb6a8d2a084f4cb4afe5e0665507f8",
                paraspirifer
        ))

        watch.stop()
        logger.info("Init lab7_q3b end")

        // Lab 7 Q3C
        watch.start("Init lab7_q3c")
        logger.info("Init lab7_q3c start")

        // Fossils
        val lab7_q3c = fossilRepository.save(Fossil(
                "lab7_q3c",
                "Lab 7 Question 3C Dictyoclostus",
                "f1c39c6416cf4bc0b0e16945ada97366",
                dictyoclostus
        ))

        watch.stop()
        logger.info("Init lab7_q3c end")

        // Lab 7 Q3D
        watch.start("Init lab7_q3d")
        logger.info("Init lab7_q3d start")

        // Fossils
        val lab7_q3d = fossilRepository.save(Fossil(
                "lab7_q3d",
                "Lab 7 Question 3D Hebertella",
                "f2c0c44b2e4e4ffba1e7e8e368090876",
                hebertella
        ))

        watch.stop()
        logger.info("Init lab7_q3d end")

        // Lab 7 Q5A
        watch.start("Init lab7_q5a")
        logger.info("Init lab7_q5a start")

        // Fossils
        val lab7_q5a = fossilRepository.save(Fossil(
                "lab7_q5a",
                "Lab 7 Question 5A Rafinesquina",
                "faf5fe4b50b84bd595a883af4bcbc1cd",
                rafinesquina
        ))

        watch.stop()
        logger.info("Init lab7_q5a end")

        // Lab 7 Q5B
        watch.start("Init lab7_q5b")
        logger.info("Init lab7_q5b start")

        // Fossils
        val lab7_q5b = fossilRepository.save(Fossil(
                "lab7_q5b",
                "Lab 7 Question 5B Orthorhyncula",
                "90f851c243344c258ea0c721ed5fe8e6",
                orthorhyncula
        ))

        watch.stop()
        logger.info("Init lab7_q5b end")

        // Lab 8 Q2B
        watch.start("Init lab8_q2b")
        logger.info("Init lab8_q2b start")

        // Fossils
        val lab8_q2b = fossilRepository.save(Fossil(
                "lab8_q2b",
                "Lab 8 Question 2B Hederella canadensis",
                "2dc13b0ab9ba4a9d9425d1561eada85c",
                canadensis
        ))

        watch.stop()
        logger.info("Init lab8_q2b end")

        // Lab 8 Q2C
        watch.start("Init lab8_q2c")
        logger.info("Init lab8_q2c start")

        // Fossils
        val lab8_q2c = fossilRepository.save(Fossil(
                "lab8_q2c",
                "Lab 8 Question 2C Constellaria",
                "4eb8d0beeed8489285a654b292a04de6",
                constellaria
        ))

        watch.stop()
        logger.info("Init lab8_q2c end")

        // Lab 8 Q2D
        watch.start("Init lab8_q2d")
        logger.info("Init lab8_q2d start")

        // Fossils
        val lab8_q2d = fossilRepository.save(Fossil(
                "lab8_q2d",
                "Lab 8 Question 2D Archimedes wortheni",
                "f7032fc103ac49eda3b57e1d9f0eaa5d",
                wortheni
        ))

        watch.stop()
        logger.info("Init lab8_q2d end")

        // Lab 8 Q3A
        watch.start("Init lab8_q3a")
        logger.info("Init lab8_q3a start")

        // Fossils
        val lab8_q3a = fossilRepository.save(Fossil(
                "lab8_q3a",
                "Lab 8 Question 3A Bathyuriscus formosus",
                "0308f81c0a2842f7a3234870bd043f08",
                formosus
        ))

        watch.stop()
        logger.info("Init lab8_q3a end")

        // Lab 8 Q3B
        watch.start("Init lab8_q3b")
        logger.info("Init lab8_q3b start")

        // Fossils
        val lab8_q3b = fossilRepository.save(Fossil(
                "lab8_q3b",
                "Lab 8 Question 3B Elrathia",
                "646a7175afc9442997116f221b1eae95",
                elrathia
        ))

        watch.stop()
        logger.info("Init lab8_q3b end")

        // Lab 8 Q3C
        watch.start("Init lab8_q3c")
        logger.info("Init lab8_q3c start")

        // Fossils
        val lab8_q3c = fossilRepository.save(Fossil(
                "lab8_q3c",
                "Lab 8 Question 3C Peronopsis interstricus",
                "5c768fb4445d4c27b2eac126ecd17ee2",
                interstricus
        ))

        watch.stop()
        logger.info("Init lab8_q3c end")

        // Lab 8 Q3D
        watch.start("Init lab8_q3d")
        logger.info("Init lab8_q3d start")

        // Fossils
        val lab8_q3d = fossilRepository.save(Fossil(
                "lab8_q3d",
                "Lab 8 Question 3D Dikelocephalus minnesoten",
                "2400d2a26dbb45b0b1a922fa8e902345",
                minnesoten
        ))

        watch.stop()
        logger.info("Init lab8_q3d end")

        // Lab 8 Q3E
        watch.start("Init lab8_q3e")
        logger.info("Init lab8_q3e start")

        // Fossils
        val lab8_q3e = fossilRepository.save(Fossil(
                "lab8_q3e",
                "Lab 8 Question 3E Phacops rana",
                "dd3a2de56332489d8ce02c218481cecf",
                rana
        ))

        watch.stop()
        logger.info("Init lab8_q3e end")

        // Lab 8 Q3F
        watch.start("Init lab8_q3f")
        logger.info("Init lab8_q3f start")

        // Fossils
        val lab8_q3f = fossilRepository.save(Fossil(
                "lab8_q3f",
                "Lab 8 Question 3F Calymene meeki",
                "f6aa113119894b13b58e3ca267dd7a35",
                meeki
        ))

        watch.stop()
        logger.info("Init lab8_q3f end")

        // Lab 8 Q5
        watch.start("Init lab8_q5")
        logger.info("Init lab8_q5 start")

        // Fossils
        val lab8_q5 = fossilRepository.save(Fossil(
                "lab8_q5",
                "Lab 8 Question 5 Balanus",
                "2445f38746964923a893a4db80e3f4b2",
                balanus
        ))

        watch.stop()
        logger.info("Init lab8_q5 end")

        // Lab 9 Q1C
        watch.start("Init lab9_q1c")
        logger.info("Init lab9_q1c start")

        // Fossils
        val lab9_q1c = fossilRepository.save(Fossil(
                "lab9_q1c",
                "Lab 9 Question 1C Dentalium",
                "e168f208a3c644a480409690b2ed72b6",
                dentalium
        ))

        watch.stop()
        logger.info("Init lab9_q1c end")

        // Lab 9 Q2A
        watch.start("Init lab9_q2a")
        logger.info("Init lab9_q2a start")

        // Fossils
        val lab9_q2a = fossilRepository.save(Fossil(
                "lab9_q2a",
                "Lab 9 Question 2A Busycon",
                "2b5454372cd8480b8382edfe81c05709",
                busycon
        ))

        watch.stop()
        logger.info("Init lab9_q2a end")

        // Lab 9 Q2B
        watch.start("Init lab9_q2b")
        logger.info("Init lab9_q2b start")

        // Fossils
        val lab9_q2b = fossilRepository.save(Fossil(
                "lab9_q2b",
                "Lab 9 Question 2B Amphiscapha",
                "68c1e92232244547a2d9c5dc91833d0d",
                amphiscapha
        ))

        watch.stop()
        logger.info("Init lab9_q2b end")

        // Lab 9 Q2C
        watch.start("Init lab9_q2c")
        logger.info("Init lab9_q2c start")

        // Fossils
        val lab9_q2c = fossilRepository.save(Fossil(
                "lab9_q2c",
                "Lab 9 Question 2C Fissurella",
                "0f07721af64c4dcfa55e19d1f187390b",
                fissurella
        ))

        watch.stop()
        logger.info("Init lab9_q2c end")

        // Lab 9 Q2D
        watch.start("Init lab9_q2d")
        logger.info("Init lab9_q2d start")

        // Fossils
        val lab9_q2d = fossilRepository.save(Fossil(
                "lab9_q2d",
                "Lab 9 Question 2D Oliva",
                "b139fac56509478a9781888cc1229ee9",
                oliva
        ))

        watch.stop()
        logger.info("Init lab9_q2d end")

        // Lab 9 Q2E
        watch.start("Init lab9_q2e")
        logger.info("Init lab9_q2e start")

        // Fossils
        val lab9_q2e = fossilRepository.save(Fossil(
                "lab9_q2e",
                "Lab 9 Question 2E Polinices",
                "9d6f8adc445e4ae6bb4b3aa0fe5b2a3b",
                polinices
        ))

        watch.stop()
        logger.info("Init lab9_q2e end")

        // Lab 9 Q2F
        watch.start("Init lab9_q2f")
        logger.info("Init lab9_q2f start")

        // Fossils
        val lab9_q2f = fossilRepository.save(Fossil(
                "lab9_q2f",
                "Lab 9 Question 2F Turritella",
                "7770f0b46d4a4d9a8fc87b60aa0ce961",
                turritella
        ))

        watch.stop()
        logger.info("Init lab9_q2f end")

        // Lab 9 Q3A
        watch.start("Init lab9_q3a")
        logger.info("Init lab9_q3a start")

        // Fossils
        val lab9_q3a = fossilRepository.save(Fossil(
                "lab9_q3a",
                "Lab 9 Question 3A Venus",
                "6478624af41445b98b882a97454e2d2e",
                venus
        ))

        watch.stop()
        logger.info("Init lab9_q3a end")

        // Lab 9 Q3B
        watch.start("Init lab9_q3b")
        logger.info("Init lab9_q3b start")

        // Fossils
        val lab9_q3b = fossilRepository.save(Fossil(
                "lab9_q3b",
                "Lab 9 Question 3B Unio",
                "4ca15aa6e70d4d36aa00e0107b0f260c",
                unio
        ))

        watch.stop()
        logger.info("Init lab9_q3b end")

        // Lab 9 Q3C
        watch.start("Init lab9_q3c")
        logger.info("Init lab9_q3c start")

        // Fossils
        val lab9_q3c = fossilRepository.save(Fossil(
                "lab9_q3c",
                "Lab 9 Question 3C Arca",
                "52536fbb1cd1469da4e554328cfedb18",
                arca
        ))

        watch.stop()
        logger.info("Init lab9_q3c end")

        // Lab 9 Q3D
        watch.start("Init lab9_q3d")
        logger.info("Init lab9_q3d start")

        // Fossils
        val lab9_q3d = fossilRepository.save(Fossil(
                "lab9_q3d",
                "Lab 9 Question 3D Exogyra",
                "e9dedafb48a6444ea5d743ae090add26",
                exogyra
        ))

        watch.stop()
        logger.info("Init lab9_q3d end")

        // Lab 9 Q3E
        watch.start("Init lab9_q3e")
        logger.info("Init lab9_q3e start")

        // Fossils
        val lab9_q3e = fossilRepository.save(Fossil(
                "lab9_q3e",
                "Lab 9 Question 3E Truncilla",
                "0632d604b8b640a4a89f558f75e50deb",
                truncilla
        ))

        watch.stop()
        logger.info("Init lab9_q3e end")

        // Lab 9 Q3F
        watch.start("Init lab9_q3f")
        logger.info("Init lab9_q3f start")

        // Fossils
        val lab9_q3f = fossilRepository.save(Fossil(
                "lab9_q3f",
                "Lab 9 Question 3F Mytilus",
                "ae56c39f11d64fde8d85ac61d02bbaf3",
                mytilus
        ))

        watch.stop()
        logger.info("Init lab9_q3f end")

        // Lab 9 Q3G
        watch.start("Init lab9_q3g")
        logger.info("Init lab9_q3g start")

        // Fossils
        val lab9_q3g = fossilRepository.save(Fossil(
                "lab9_q3g",
                "Lab 9 Question 3G Mactra",
                "043247604bd946f1b281ace69dc006ab",
                mactra
        ))

        watch.stop()
        logger.info("Init lab9_q3g end")

        // Lab 9 Q7A
        watch.start("Init lab9_q7a")
        logger.info("Init lab9_q7a start")

        // Fossils
        val lab9_q7a = fossilRepository.save(Fossil(
                "lab9_q7a",
                "Lab 9 Question 7A Pachyteuthis",
                "ea9f28443660454893f546e113df8e6f",
                pachyteuthis
        ))

        watch.stop()
        logger.info("Init lab9_q7a end")

        // Lab 9 Q7B
        watch.start("Init lab9_q7b")
        logger.info("Init lab9_q7b start")

        // Fossils
        val lab9_q7b = fossilRepository.save(Fossil(
                "lab9_q7b",
                "Lab 9 Question 7B Baculites",
                "f9145c2075f24f7ab8ac1666fe7559b7",
                baculites
        ))

        watch.stop()
        logger.info("Init lab9_q7b end")

        // Lab 9 Q7D
        watch.start("Init lab9_q7d")
        logger.info("Init lab9_q7d start")

        // Fossils
        val lab9_q7d = fossilRepository.save(Fossil(
                "lab9_q7d",
                "Lab 9 Question 7D Acanthoscaphites",
                "5faa147f1e484b5a873c5d4c0892b019",
                acanthoscaphites
        ))

        watch.stop()
        logger.info("Init lab9_q7d end")

        // Lab 10 Q1A
        watch.start("Init lab10_q1a")
        logger.info("Init lab10_q1a start")

        // Fossils
        val lab10_q1a = fossilRepository.save(Fossil(
                "lab10_q1a",
                "Lab 10 Question 1A Dorycrinus",
                "c8b889b010a94527a640d3fbe3dec117",
                dorycrinus
        ))

        watch.stop()
        logger.info("Init lab10_q1a end")

        // Lab 10 Q1B
        watch.start("Init lab10_q1b")
        logger.info("Init lab10_q1b start")

        // Fossils
        val lab10_q1b = fossilRepository.save(Fossil(
                "lab10_q1b",
                "Lab 10 Question 1B Cactocrinus",
                "a52364c8c78746a8b8623cf96a5a65e8",
                cactocrinus
        ))

        watch.stop()
        logger.info("Init lab10_q1b end")

        // Lab 10 Q2
        watch.start("Init lab10_q2")
        logger.info("Init lab10_q2 start")

        // Fossils
        val lab10_q2 = fossilRepository.save(Fossil(
                "lab10_q2",
                "Lab 10 Question 2 Orophocrinus",
                "cc2456f2efbb49dc81a145d24c977417",
                orophocrinus
        ))

        watch.stop()
        logger.info("Init lab10_q2 end")

        // Lab 10 Q3A
        watch.start("Init lab10_q3a")
        logger.info("Init lab10_q3a start")

        // Fossils
        val lab10_q3a = fossilRepository.save(Fossil(
                "lab10_q3a",
                "Lab 10 Question 3A Hempineustes",
                "11ae0010584c4b768368d452ccc3dd6c",
                listOf(
                        hempineustes
                )
        ))

        watch.stop()
        logger.info("Init lab10_q3a end")

        // Lab 10 Q3B
        watch.start("Init lab10_q3b")
        logger.info("Init lab10_q3b start")

        // Fossils
        val lab10_q3b = fossilRepository.save(Fossil(
                "lab10_q3b",
                "Lab 10 Question 3B Diplothecanthus",
                "5c89812a4ad7478c984a2baba27766b0",
                diplothecanthus
        ))

        watch.stop()
        logger.info("Init lab10_q3b end")

        // Lab 10 Q5A
        watch.start("Init lab10_q5a")
        logger.info("Init lab10_q5a start")

        // Fossils
        val lab10_q5a = fossilRepository.save(Fossil(
                "lab10_q5a",
                "Lab 10 Question 5A Pteraster",
                "e4b068636232465e9080ac6ad267f2ca",
                pteraster
        ))

        watch.stop()
        logger.info("Init lab10_q5a end")

        // Lab 10 Q5B
        watch.start("Init lab10_q5b")
        logger.info("Init lab10_q5b start")

        // Fossils
        val lab10_q5b = fossilRepository.save(Fossil(
                "lab10_q5b",
                "Lab 10 Question 5B Thyone",
                "42f51363ffc44cb6ab451f20c3966d9e",
                thyone
        ))

        watch.stop()
        logger.info("Init lab10_q5b end")

        // Lab 10 Q5C
        watch.start("Init lab10_q5c")
        logger.info("Init lab10_q5c start")

        // Fossils
        val lab10_q5c = fossilRepository.save(Fossil(
                "lab10_q5c",
                "Lab 10 Question 5C Echinarachnius",
                "921d58b32ea14f4fbbfe643935a861fa",
                echinarachnius
        ))

        watch.stop()
        logger.info("Init lab10_q5c end")

        logger.info(watch.prettyPrint())

        //logger.info(lab2_q1d.toString())
    }
}
