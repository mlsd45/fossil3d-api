package com.skaggsm.fossil3d.api.model.geology.temporal.divisions

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

/**
 * Created by Mitchell Skaggs on 2/24/2019.
 */
@RepositoryRestResource(excerptProjection = TimeDivisionWithTimeScale::class)
interface TimeDivisionRepository : PagingAndSortingRepository<TimeDivision, String> {
    @RestResource(exported = false)
    override fun <S : TimeDivision?> save(entity: S): S

    @RestResource(exported = false)
    override fun <S : TimeDivision?> saveAll(entities: MutableIterable<S>): MutableIterable<S>

    @RestResource(exported = false)
    override fun delete(entity: TimeDivision)

    @RestResource(exported = false)
    override fun deleteById(id: String)

    @RestResource(exported = false)
    override fun deleteAll()

    @RestResource(exported = false)
    override fun deleteAll(entities: MutableIterable<TimeDivision>)
}