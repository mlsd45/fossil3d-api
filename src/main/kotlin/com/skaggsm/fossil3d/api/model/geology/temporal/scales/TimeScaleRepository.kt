package com.skaggsm.fossil3d.api.model.geology.temporal.scales

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

/**
 * Created by Mitchell Skaggs on 2/24/2019.
 */
@RepositoryRestResource
interface TimeScaleRepository : PagingAndSortingRepository<TimeScale, String> {
    @RestResource(exported = false)
    override fun <S : TimeScale?> save(entity: S): S

    @RestResource(exported = false)
    override fun <S : TimeScale?> saveAll(entities: MutableIterable<S>): MutableIterable<S>

    @RestResource(exported = false)
    override fun delete(entity: TimeScale)

    @RestResource(exported = false)
    override fun deleteById(id: String)

    @RestResource(exported = false)
    override fun deleteAll()

    @RestResource(exported = false)
    override fun deleteAll(entities: MutableIterable<TimeScale>)
}