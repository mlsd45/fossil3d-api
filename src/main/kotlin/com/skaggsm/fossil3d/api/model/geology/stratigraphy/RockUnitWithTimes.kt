package com.skaggsm.fossil3d.api.model.geology.stratigraphy

import com.skaggsm.fossil3d.api.model.geology.temporal.divisions.TimeDivisionWithTimeScale
import org.springframework.data.rest.core.config.Projection

/**
 * Created by Mitchell Skaggs on 3/16/2019.
 */
@Projection(types = [RockUnit::class])
interface RockUnitWithTimes {
    val id: String
    val location: String
    val times: List<TimeDivisionWithTimeScale>
}