package com.skaggsm.fossil3d.api.model.geology.samples.fossils

import com.skaggsm.fossil3d.api.model.geology.stratigraphy.RockUnit
import com.skaggsm.fossil3d.api.model.taxonomy.taxa.Taxon
import io.crnk.core.resource.annotations.JsonApiResource
import javax.persistence.*

/**
 * Created by Mitchell Skaggs on 2/21/2019.
 */
@JsonApiResource(type = "fossil", patchable = false, postable = false, deletable = false)
@Entity
data class Fossil(
        @Id
        val id: String,

        @Column(nullable = false)
        val name: String,

        @Column(nullable = true)
        val modelId: String?,

        @ManyToMany(fetch = FetchType.LAZY)
        @JoinTable(name = "fossil_taxon")
        val taxa: List<Taxon> = emptyList(),

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinTable(name = "fossil_rock_unit")
        val rockUnit: RockUnit? = null,

        @Column(nullable = true)
        val collectionNumber: Long? = null
) {
    constructor(id: String, name: String, modelId: String?, seedTaxa: Taxon, rockUnit: RockUnit? = null, collectionNumber: Long? = null)
            : this(id, name, modelId, seedTaxa.allParents, rockUnit, collectionNumber)
}

val Taxon.allParents: List<Taxon>
    get() {
        val parents = mutableListOf<Taxon>()
        var current: Taxon? = this

        while (current != null) {
            parents += current
            current = current.parent
        }

        return parents.toSortedSet().toList()
    }
