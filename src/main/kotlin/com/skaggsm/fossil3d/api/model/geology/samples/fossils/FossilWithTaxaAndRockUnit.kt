package com.skaggsm.fossil3d.api.model.geology.samples.fossils

import com.skaggsm.fossil3d.api.model.geology.stratigraphy.RockUnitWithTimes
import com.skaggsm.fossil3d.api.model.taxonomy.taxa.TaxonWithRankOrdering
import org.springframework.data.rest.core.config.Projection

/**
 * Created by Mitchell Skaggs on 3/16/2019.
 */
@Projection(types = [Fossil::class])
interface FossilWithTaxaAndRockUnit {
    val id: String
    val name: String
    val modelId: String
    val taxa: List<TaxonWithRankOrdering>
    val rockUnit: RockUnitWithTimes?
    val collectionNumber: Long?
}