package com.skaggsm.fossil3d.api.model.taxonomy.ranks

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

/**
 * Created by Mitchell Skaggs on 2/22/2019.
 */
@RepositoryRestResource
interface RankRepository : PagingAndSortingRepository<Rank, String> {
    @RestResource(exported = false)
    override fun <S : Rank?> save(entity: S): S

    @RestResource(exported = false)
    override fun <S : Rank?> saveAll(entities: MutableIterable<S>): MutableIterable<S>

    @RestResource(exported = false)
    override fun delete(entity: Rank)

    @RestResource(exported = false)
    override fun deleteById(id: String)

    @RestResource(exported = false)
    override fun deleteAll()

    @RestResource(exported = false)
    override fun deleteAll(entities: MutableIterable<Rank>)
}