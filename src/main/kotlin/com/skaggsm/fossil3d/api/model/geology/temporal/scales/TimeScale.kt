package com.skaggsm.fossil3d.api.model.geology.temporal.scales

import com.skaggsm.fossil3d.api.model.geology.temporal.divisions.TimeDivision
import io.crnk.core.resource.annotations.JsonApiResource
import javax.persistence.*

/**
 * Created by Mitchell Skaggs on 2/24/2019.
 */
@JsonApiResource(type = "timeScale", patchable = false, postable = false, deletable = false)
@Entity
data class TimeScale(
        @Id
        val name: String,

        @Column(nullable = false)
        val ordering: Long
) {
    @OneToMany(mappedBy = "scale", fetch = FetchType.LAZY)
    val timeDivisions: List<TimeDivision> = emptyList()
}
