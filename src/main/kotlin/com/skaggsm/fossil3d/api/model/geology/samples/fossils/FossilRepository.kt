package com.skaggsm.fossil3d.api.model.geology.samples.fossils

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

/**
 * Created by Mitchell Skaggs on 2/21/2019.
 */
@RepositoryRestResource(excerptProjection = FossilWithTaxaAndRockUnit::class)
interface FossilRepository : PagingAndSortingRepository<Fossil, String> {
    @RestResource(exported = false)
    override fun <S : Fossil?> save(entity: S): S

    @RestResource(exported = false)
    override fun <S : Fossil?> saveAll(entities: MutableIterable<S>): MutableIterable<S>

    @RestResource(exported = false)
    override fun delete(entity: Fossil)

    @RestResource(exported = false)
    override fun deleteById(id: String)

    @RestResource(exported = false)
    override fun deleteAll()

    @RestResource(exported = false)
    override fun deleteAll(entities: MutableIterable<Fossil>)
}
