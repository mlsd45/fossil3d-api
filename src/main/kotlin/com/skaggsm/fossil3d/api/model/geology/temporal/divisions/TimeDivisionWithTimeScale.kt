package com.skaggsm.fossil3d.api.model.geology.temporal.divisions

import com.skaggsm.fossil3d.api.model.geology.temporal.scales.TimeScale
import org.springframework.data.rest.core.config.Projection

/**
 * Created by Mitchell Skaggs on 3/16/2019.
 */
@Projection(types = [TimeDivision::class])
interface TimeDivisionWithTimeScale {
    val name: String
    val scale: TimeScale
}