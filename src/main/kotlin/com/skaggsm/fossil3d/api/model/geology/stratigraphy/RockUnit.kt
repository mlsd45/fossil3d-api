package com.skaggsm.fossil3d.api.model.geology.stratigraphy

import com.skaggsm.fossil3d.api.model.geology.samples.fossils.Fossil
import com.skaggsm.fossil3d.api.model.geology.temporal.divisions.TimeDivision
import io.crnk.core.resource.annotations.JsonApiResource
import javax.persistence.*

/**
 * Created by Mitchell Skaggs on 2/24/2019.
 */
@JsonApiResource(type = "rockUnit", patchable = false, postable = false, deletable = false)
@Entity
data class RockUnit(
        @Id
        val id: String,

        @Column(nullable = false)
        val location: String,

        @ManyToMany(fetch = FetchType.LAZY)
        @JoinTable(name = "rock_unit_time_division")
        val times: List<TimeDivision> = emptyList()
) {
    @OneToMany(mappedBy = "rockUnit", fetch = FetchType.LAZY)
    val fossils: List<Fossil> = emptyList()
}
