package com.skaggsm.fossil3d.api.model.taxonomy.ranks

import com.skaggsm.fossil3d.api.model.taxonomy.taxa.Taxon
import io.crnk.core.resource.annotations.JsonApiResource
import javax.persistence.*

/**
 * Created by Mitchell Skaggs on 2/22/2019.
 */
@JsonApiResource(type = "rank", patchable = false, postable = false, deletable = false)
@Entity
@Table(name = "`rank`")
data class Rank(
        @Id
        @Column(nullable = false)
        val name: String,

        @Column(nullable = false)
        val ordering: Long
) : Comparable<Rank> {
    override fun compareTo(other: Rank): Int {
        return this.ordering.compareTo(other.ordering)
    }

    @OneToMany(mappedBy = "rank", fetch = FetchType.LAZY)
    val taxa: List<Taxon> = emptyList()
}
