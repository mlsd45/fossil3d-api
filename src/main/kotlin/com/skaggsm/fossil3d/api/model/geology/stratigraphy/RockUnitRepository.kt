package com.skaggsm.fossil3d.api.model.geology.stratigraphy

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

/**
 * Created by Mitchell Skaggs on 2/24/2019.
 */

@RepositoryRestResource(excerptProjection = RockUnitWithTimes::class)
interface RockUnitRepository : PagingAndSortingRepository<RockUnit, String> {
    @RestResource(exported = false)
    override fun <S : RockUnit?> save(entity: S): S

    @RestResource(exported = false)
    override fun <S : RockUnit?> saveAll(entities: MutableIterable<S>): MutableIterable<S>

    @RestResource(exported = false)
    override fun delete(entity: RockUnit)

    @RestResource(exported = false)
    override fun deleteById(id: String)

    @RestResource(exported = false)
    override fun deleteAll()

    @RestResource(exported = false)
    override fun deleteAll(entities: MutableIterable<RockUnit>)
}