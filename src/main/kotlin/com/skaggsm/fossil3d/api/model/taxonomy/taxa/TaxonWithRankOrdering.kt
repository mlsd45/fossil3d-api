package com.skaggsm.fossil3d.api.model.taxonomy.taxa

import org.springframework.beans.factory.annotation.Value
import org.springframework.data.rest.core.config.Projection

/**
 * Created by Mitchell Skaggs on 3/12/2019.
 */
@Projection(types = [Taxon::class])
interface TaxonWithRankOrdering {
    val name: String

    @get:Value("#{target.rank.name}")
    val rankName: String

    @get:Value("#{target.rank.ordering}")
    val rankOrdering: Long
}