package com.skaggsm.fossil3d.api.model.taxonomy.taxa

import com.skaggsm.fossil3d.api.model.geology.samples.fossils.Fossil
import com.skaggsm.fossil3d.api.model.taxonomy.ranks.Rank
import io.crnk.core.resource.annotations.JsonApiResource
import javax.persistence.*

/**
 * Created by Mitchell Skaggs on 2/22/2019.
 */
@JsonApiResource(type = "taxon", patchable = false, postable = false, deletable = false)
@Entity
data class Taxon(
        @Id
        @Column(nullable = false)
        val name: String,

        @ManyToOne
        @JoinTable(name = "taxon_rank")
        val rank: Rank,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinTable(name = "taxon_parent")
        val parent: Taxon? = null
) : Comparable<Taxon> {
    override fun compareTo(other: Taxon): Int {
        return this.rank.compareTo(other.rank)
    }

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    val children: List<Taxon> = emptyList()

    @ManyToMany(mappedBy = "taxa", fetch = FetchType.LAZY)
    val members: List<Fossil> = emptyList()
}
