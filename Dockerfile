# DEPRECATED, using Gradle `bootBuildImage` now

# build
FROM eclipse-temurin:17-jdk-alpine as builder
RUN mkdir /opt/app
WORKDIR /opt/app
COPY gradlew* ./
COPY gradle/ gradle/
COPY settings.gradle* ./
RUN ./gradlew -q
COPY *.kts ./
RUN ./gradlew -q dependencies
COPY src src
RUN ./gradlew -q bootJar
RUN mkdir -p build/libs/dependency && (cd build/libs/dependency; jar -xf ../*.jar)

# production
FROM eclipse-temurin:17-jre-alpine
RUN mkdir /opt/app
WORKDIR /opt/app
EXPOSE 80
VOLUME /tmp
ARG DEPENDENCY=/opt/app/build/libs/dependency
COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib /opt/app/lib
COPY --from=builder ${DEPENDENCY}/META-INF /opt/app/META-INF
COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes /opt/app
ENTRYPOINT ["java","-cp",".:lib/*","com.skaggsm.fossil3d.api.FossilApiApplicationKt"]
