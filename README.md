# Fossils 3D Backend

## Development

Run `gradlew build` to install all dependencies and build.

Run `gradlew bootRun --args='--spring.profiles.active=dev'` to start the development server at `localhost:8080`.

When the `dev` profile is active, an in-memory database is used instead of the MariaDB database.

## Deployment

Deploy with `docker-compose up -d` in the project root.

The provided `docker-compose.yml` first builds a production version of the app and copies the result to a JVM container and then deploys the app and a MariaDB database.

A network called `web` is assumed to exist.
